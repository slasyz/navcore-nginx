Installing
==========

Quick way to start nginx with our module.

- Download nginx sources from mercurial: `hg clone http://hg.nginx.org/nginx`
- `cd ~/path/to/nginx`
- `./auto/configure --add-module=/path/to/navcore-nginx --prefix=/path/to/navcore-nginx/_prefix --with-http_ssl_module --with-stream --with-ld-opt=" -ljansson"`
- `make && ./objs/nginx`

Now you can open http://localhost:8123/ and https://localhost:8124/ in browser.
