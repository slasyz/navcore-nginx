#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>
#include <sys/types.h>
#include <stdio.h>
#include <jansson.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

static char *ngx_http_navcore_loopback(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);
static ngx_int_t ngx_http_navcore_handler(ngx_http_request_t *r);
static void *ngx_http_navcore_create_loc_conf(ngx_conf_t *cf);

/**
 * This module provided directive: navcore.
 *
 */
static ngx_command_t ngx_http_navcore_commands[] = {

    { ngx_string("navcore_loopback"), /* directive */
      NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1, /* location context and takes
                                            no arguments */
      ngx_http_navcore_loopback, /* configuration setup function */
      0, /* No offset. Only one context is supported. */
      0, /* No offset when storing the module configuration on struct. */
      NULL},

    ngx_null_command /* command termination */
};

/* The module context. */
static ngx_http_module_t ngx_http_navcore_module_ctx = {
        NULL, /* preconfiguration */
        NULL, /* postconfiguration */

        NULL, /* create main configuration */
        NULL, /* init main configuration */

        NULL, /* create server configuration */
        NULL, /* merge server configuration */

        ngx_http_navcore_create_loc_conf, /* create location configuration */
        NULL /* merge location configuration */
};

/* Module definition. */
ngx_module_t ngx_http_navcore_module = {
        NGX_MODULE_V1,
        &ngx_http_navcore_module_ctx, /* module context */
        ngx_http_navcore_commands, /* module directives */
        NGX_HTTP_MODULE, /* module type */
        NULL, /* init master */
        NULL, /* init module */
        NULL, /* init process */
        NULL, /* init thread */
        NULL, /* exit thread */
        NULL, /* exit process */
        NULL, /* exit master */
        NGX_MODULE_V1_PADDING
};

#define MAX_KEY_LENGTH 20
#define MAX_VALUE_LENGTH 10 // 2147483647 or AB_REQUEST
#define MAX_STRING_LENGTH 20

typedef struct {
    int loopback_fd;
    char *loopback_addr;
    FILE *loopback_fp;
} ngx_http_navcore_loc_conf_t;

typedef enum {KEY, VALUE} parse_state_t;

typedef struct {
    char *transaction_type;
    int request_id;
    char *branch_number;
    char *basic_account_number;
    char *account_suffix;

    int current_symbol_code; // In case if we get something like %AB, and here we
                             // store decimal representation of hexadecimal value AB
    int current_symbol_position; // 0 if we're not parsing hexadecimal value now
                                 // 1 if we're expecting A
                                 // 2 if we're expecting B
    char current_string[MAX_STRING_LENGTH];
    size_t current_string_length;
    char current_key[MAX_STRING_LENGTH];
    size_t current_key_length;
    parse_state_t current_state;
} ngx_http_navcore_ctx_t;

static u_char transaction_type_key[] = "transaction_type";
static u_char request_id_key[] = "request_id";
static u_char branch_number_key[] = "branch_number";
static u_char basic_account_number_key[] = "basic_account_number";
static u_char account_suffix_key[] = "account_suffix";

// atoi for fixed-length string
int fixed_atoi(char str[], size_t str_length) {
    unsigned int res = 0;

    for (unsigned int i = 0; i < str_length; i++) {
        if (str[i] < 48 || str[i] > 57) // Non-number character
            return -1;

        res = res*10 + (str[i] - 48);
    }

    return res;
}

void err_log(const char *msg, ngx_http_request_t *r) {
    ngx_log_error(NGX_LOG_ERR, r->connection->log, 0, msg);

    r->headers_out.status = NGX_HTTP_INTERNAL_SERVER_ERROR; /* 200 status code */
    r->headers_out.content_length_n = 0;
    ngx_http_send_header(r); /* Send the headers */
}

ngx_int_t save_key_value_pair(ngx_http_request_t *r)
{
    ngx_http_navcore_ctx_t *ctx;

    ctx = ngx_http_get_module_ctx(r, ngx_http_navcore_module);

    if (ctx->current_key_length == 16) {
        if (memcmp(ctx->current_key, transaction_type_key, 16) == 0) {
            // "transaction_type" key
            strncpy(ctx->transaction_type, ctx->current_string, ctx->current_string_length);
        } else {
            err_log("strncpy failed, length=16", r);
            return NGX_HTTP_BAD_REQUEST;
        }
    } else if (ctx->current_key_length == 10) {
        if (memcmp(ctx->current_key, request_id_key, 10) == 0) {
            // "request_id" key
            ctx->request_id = fixed_atoi(ctx->current_string, ctx->current_string_length);
        } else {
            err_log("strncpy failed, length=10", r);
            return NGX_HTTP_BAD_REQUEST;
        }
    } else if (ctx->current_key_length == 13) {
        if (memcmp(ctx->current_key, branch_number_key, 13) == 0) {
            // "branch_number" key
            strncpy(ctx->branch_number, ctx->current_string, ctx->current_string_length);
        } else {
            err_log("strncpy failed, length=13", r);
            return NGX_HTTP_BAD_REQUEST;
        }
    } else if (ctx->current_key_length == 20) {
        if (memcmp(ctx->current_key, basic_account_number_key, 20) == 0) {
            // "basic_account_number" key
            strncpy(ctx->basic_account_number, ctx->current_string, ctx->current_string_length);
        } else {
            err_log("strncpy failed, length=20", r);
            return NGX_HTTP_BAD_REQUEST;
        }
    } else if (ctx->current_key_length == 14) {
        if (memcmp(ctx->current_key, account_suffix_key, 14) == 0) {
            // "account_suffix" key
            strncpy(ctx->account_suffix, ctx->current_string, ctx->current_string_length);
        } else {
            err_log("strncpy failed, length=14", r);
            return NGX_HTTP_BAD_REQUEST;
        }
    } else {
        // Unrecognized key
        err_log("wrong key length", r);
        return NGX_HTTP_BAD_REQUEST;
    }

    ctx->current_state = KEY;
    ctx->current_key_length = 0;
    ctx->current_string_length = 0;
    return NGX_OK;
}

static ngx_int_t
ngx_http_navcore_body_handle(ngx_http_request_t *r)
{
    ngx_http_navcore_ctx_t     *ctx;

    ngx_chain_t *current_buffer;
    u_char *c;

    ctx = ngx_http_get_module_ctx(r, ngx_http_navcore_module);

    if (r->request_body == NULL) {
        err_log("Empty request body", r);
        return NGX_HTTP_BAD_REQUEST;
    }

    current_buffer = r->request_body->bufs;
    ctx->current_state = KEY;
    while (current_buffer != NULL) {
        if (current_buffer->buf->in_file) {
            err_log("form-input: in-file buffer found. aborted. "
                            "consider increasing your client_body_buffer_size "
                            "setting", r);

            return NGX_HTTP_INTERNAL_SERVER_ERROR;
        }

        // Read current buffer symbol by symbol
        for (c = current_buffer->buf->start; c < current_buffer->buf->last; c++) {
            switch (*c) {
                case '=' :
                    if (ctx->current_symbol_position > 0) {
                        err_log("There's \"...%=...\" or \"...%A=...\"", r);
                        return NGX_HTTP_BAD_REQUEST;
                    }
                    if (ctx->current_state == VALUE) {
                        err_log("There's \"...&param=val=...\"", r);
                        return NGX_HTTP_BAD_REQUEST;
                    }

                    // We've read another key, store it to separate variable
                    strncpy(ctx->current_key, ctx->current_string, ctx->current_string_length);
                    ctx->current_key_length = ctx->current_string_length;
                    ctx->current_string_length = 0;

                    // Start parsing value now
                    ctx->current_state = VALUE;
                    break;
                case '&' :
                    if (ctx->current_symbol_position > 0) {
                        err_log("There's \"...%&...\" or \"...%A&...\"", r);
                        return NGX_HTTP_BAD_REQUEST;
                    }

                    ngx_int_t rc = save_key_value_pair(r);
                    if (rc != NGX_OK)
                        return rc;


                    break;
                case '%' :
                    if (ctx->current_symbol_position != 0) { // 0 means "we're expecting for `%` symbol or ordinary symbol"
                        err_log("faced with %% or %A%", r);
                        return NGX_HTTP_BAD_REQUEST;
                    }

                    ctx->current_symbol_code = 0;
                    ctx->current_symbol_position = 1; // Now we're expecting [a-fA-F0-9]
                    break;
                default:
                    if (ctx->current_string_length == MAX_STRING_LENGTH) {
                        err_log("maximum key or value length is reached", r);
                        return NGX_HTTP_BAD_REQUEST;
                    }

                    // Are we parsing something like %AB?
                    if (ctx->current_symbol_position == 1) {
                        // Yes, and it's first symbol after `%`
                        if (*c >= 48 && *c <= 57) // 0-9
                            ctx->current_symbol_code = 16 * (*c - 48);
                        else if (*c >= 65 && *c <= 70) // A-F
                            ctx->current_symbol_code = 16 * (*c - 65 + 10);
                        else if (*c >= 97 && *c <= 102) // a-f
                            ctx->current_symbol_code = 16 * (*c - 97 + 10);
                        else {
                            err_log("non-[a-fA-F0-9] after %", r);
                            return NGX_HTTP_BAD_REQUEST;
                        }

                        ctx->current_symbol_position++;

                    } else if (ctx->current_symbol_position == 2) {
                        // Yes, and it's second symbol after `%`
                        if (*c >= 48 && *c <= 57) // 0-9
                            ctx->current_symbol_code = *c - 48;
                        else if (*c >= 65 && *c <= 70) // A-F
                            ctx->current_symbol_code = *c - 65 + 10;
                        else if (*c >= 97 && *c <= 102) // a-f
                            ctx->current_symbol_code = *c - 97 + 10;
                        else {
                            err_log("non-[a-fA-F0-9] after %A", r);
                            return NGX_HTTP_BAD_REQUEST;
                        }

                        ctx->current_string[ctx->current_string_length] = (char) ctx->current_symbol_code;
                        ctx->current_symbol_position = 0;

                    } else {
                        // Nope, we're expecting ordinary symbol
                        ctx->current_string[ctx->current_string_length] = *c;
                        ctx->current_string_length++;
                    }
            }
        }

        current_buffer = current_buffer->next;
    }

    if (ctx->current_symbol_position > 0) {
        err_log("Request body ends with `%` or `%A`", r);
        return NGX_HTTP_BAD_REQUEST;
    }

    if (ctx->current_state != VALUE) {
        err_log("Request body ends with ...&keyname", r);
        return NGX_HTTP_BAD_REQUEST;
    }

    if (save_key_value_pair(r) != NGX_OK)
        return NGX_HTTP_BAD_REQUEST;

    return NGX_OK;
}

static void ngx_http_navcore_post_read(ngx_http_request_t *r)
{
    ngx_http_navcore_ctx_t     *ctx;
    ngx_http_navcore_loc_conf_t  *conf;

    ngx_buf_t *b;
    ngx_chain_t out;

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                   "http form_input post read request body");

    ctx = ngx_http_get_module_ctx(r, ngx_http_navcore_module);
    conf = ngx_http_get_module_loc_conf(r, ngx_http_navcore_module);

    ngx_int_t rc;
    rc = ngx_http_navcore_body_handle(r);

    if (rc != NGX_OK) {
        err_log("should write BAD_REQUEST", r);
        return;
    }

#if defined(nginx_version) && nginx_version >= 8011
    r->main->count--;
#endif

    /* Set the Content-Type header. */
    r->headers_out.content_type.len = sizeof("text/json") - 1;
    r->headers_out.content_type.data = (u_char *) "text/json";

    // Make request JSON
    json_t *request_json;
    json_t *request_json_payload;
    char *request_str;
    //size_t request_str_size;

    json_t *request_json_request_id, *request_json_branch_number, *request_json_basic_account_number, *request_json_account_suffix;
    json_t *request_json_transaction_type;

    request_json_request_id = json_integer(ctx->request_id);
    request_json_branch_number = json_string(ctx->branch_number);
    request_json_basic_account_number = json_string(ctx->basic_account_number);
    request_json_account_suffix = json_string(ctx->account_suffix);
    request_json_transaction_type = json_string(ctx->transaction_type);

    request_json_payload = json_object();
    json_object_set(request_json_payload, "request_id", request_json_request_id);
    json_object_set(request_json_payload, "branch_number", request_json_branch_number);
    json_object_set(request_json_payload, "basic_account_number", request_json_basic_account_number);
    json_object_set(request_json_payload, "account_suffix", request_json_account_suffix);

    request_json = json_object();
    json_object_set(request_json, "transaction_type", request_json_transaction_type);
    json_object_set(request_json, "payload", request_json_payload);

    request_str = json_dumps(request_json, JSON_INDENT(2));

    // Send this JSON message to loopback server
    ssize_t res = fprintf(conf->loopback_fp, "%s", request_str);
    fseek(conf->loopback_fp, 0, SEEK_END);
    if (res == -1) {
        printf("failed to make HTTP request: %s\n", strerror(errno));
        return;
    }
    //printf("Sent %d bytes to loopback\n", (int) res);

    u_char *reply_from_loopback;

    //printf("Getting reply from loopback\n");
    json_t *reply_from_loopback_json = json_loadf(conf->loopback_fp, JSON_DISABLE_EOF_CHECK, NULL);
    if (reply_from_loopback_json == NULL) {
        err_log("reply from loopback decoding error\n", r);
        return;
    }

    char *reply_from_loopback_char = json_dumps(reply_from_loopback_json, 0);
    reply_from_loopback = (u_char *) reply_from_loopback_char;
    res = strlen(reply_from_loopback_char);
    //printf("Got reply from loopback\n");

    /* Allocate a new buffer for sending out the reply. */
    b = ngx_pcalloc(r->pool, sizeof(ngx_buf_t));

    /* Insertion in the buffer chain. */
    out.buf = b;
    out.next = NULL; /* just one buffer */

    b->pos = reply_from_loopback; /* first position in memory of the data */
    b->last = reply_from_loopback + res; /* last position in memory of the data */
    b->memory = 1; /* content is in read-only memory */
    b->last_buf = 1; /* there will be no more buffers in the request */

    /* Sending the headers for the reply. */
    r->headers_out.status = NGX_HTTP_OK; /* 200 status code */
    /* Get the content length of the body. */
    r->headers_out.content_length_n = res;
    ngx_http_send_header(r); /* Send the headers */

    /* Send the body, and return the status code of the output filter chain. */
    ngx_http_output_filter(r, &out);

    json_decref(reply_from_loopback_json);
    json_decref(request_json_payload);
    json_decref(request_json);
    free(request_str);
    free(reply_from_loopback_char);

    json_decref(request_json_request_id);
    json_decref(request_json_branch_number);
    json_decref(request_json_basic_account_number);
    json_decref(request_json_account_suffix);
    json_decref(request_json_transaction_type);

    free(ctx->transaction_type);
    free(ctx->branch_number);
    free(ctx->basic_account_number);
    free(ctx->account_suffix);
}

static ngx_int_t ngx_http_navcore_handler(ngx_http_request_t *r)
{
    ngx_http_navcore_ctx_t *ctx;

    ngx_int_t rc;

    if (r->method != NGX_HTTP_POST) {
        err_log("Not a POST request", r);
        return NGX_HTTP_BAD_REQUEST;
    }

    ctx = ngx_pcalloc(r->pool, sizeof(ngx_http_navcore_ctx_t));
    if (ctx == NULL) {
        return NGX_ERROR;
    }

    ctx->request_id = -1;
    ctx->transaction_type = calloc(1, 10+1);
    ctx->branch_number = calloc(1, 4+1);
    ctx->basic_account_number = calloc(1, 6+1);
    ctx->account_suffix = calloc(1, 3+1);

    //ctx->current_symbol_position = 0;
    //ctx->current_string_length = 0;
    //ctx->current_key_length = 0;

    ngx_http_set_ctx(r, ctx, ngx_http_navcore_module);

    rc = ngx_http_read_client_request_body(r, ngx_http_navcore_post_read);
    //printf("rc=%d\n", (int) rc);

    if (rc == NGX_ERROR || rc >= NGX_HTTP_SPECIAL_RESPONSE) {
#if (nginx_version < 1002006) ||                                             \
        (nginx_version >= 1003000 && nginx_version < 1003009)
        r->main->count--;
#endif

        return rc;
    }

    if (rc == NGX_AGAIN) {
        return NGX_DONE;
    }

    return NGX_DONE;
} /* ngx_http_navcore_handler */


static int
connect_to_loopback(const char *addrstr)
{
    char *loopback_host = calloc(1, 128);
    int loopback_port;
    int loopback_fd;

    sscanf(addrstr, "http://%99[^:]:%d", loopback_host, &loopback_port);
    //printf("host: %s, port: %d\n", loopback_host, (uint16_t) loopback_port);

    struct hostent *hp;
    struct sockaddr_in addr;
    int on = 1;

    if((hp = gethostbyname(loopback_host)) == NULL){
        printf("connect to loopback failed: gethostbyname");
        exit(1);
    }
    bcopy(hp->h_addr, &addr.sin_addr, (size_t) hp->h_length);
    addr.sin_port = htons((uint16_t) loopback_port);
    addr.sin_family = AF_INET;
    loopback_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    setsockopt(loopback_fd, IPPROTO_TCP, TCP_NODELAY, (const char *)&on, sizeof(int));

    if(loopback_fd == -1){
        printf("connect to loopback failed: setsockopt");
        exit(1);
    }

    if(connect(loopback_fd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) == -1){
        printf("connect to loopback failed: connect");
        exit(1);
    }

    //printf("connected to loopback, fd=%d\n", loopback_fd);
    free(loopback_host);
    return loopback_fd;
}

static char *ngx_http_navcore_loopback(ngx_conf_t *cf, ngx_command_t *cmd, void *confptr)
{
    ngx_http_core_loc_conf_t *clcf; /* pointer to core location configuration */
    ngx_http_navcore_loc_conf_t *conf;

    conf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_navcore_module);

    clcf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_core_module);
    clcf->handler = ngx_http_navcore_handler;

    ngx_str_t *elts = cf->args->elts;

    conf->loopback_addr = calloc(1, elts[1].len+1);
    strncpy(conf->loopback_addr, (char *) elts[1].data, elts[1].len);
    //printf("conf->loopback_addr = %s\n", conf->loopback_addr);
    conf->loopback_fd = connect_to_loopback(conf->loopback_addr);
    conf->loopback_fp = fdopen(conf->loopback_fd, "a+");
    if (conf->loopback_fp == NULL) {
        printf("fdopen() failed\n");
        return NGX_CONF_ERROR;
    }

    //printf("fp = %p\n", conf->loopback_fp);
    free(conf->loopback_addr);
    return NGX_CONF_OK;
} /* ngx_http_navcore */

static void *
ngx_http_navcore_create_loc_conf(ngx_conf_t *cf)
{
    ngx_http_navcore_loc_conf_t  *conf;

    conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_navcore_loc_conf_t));
    if (conf == NULL) {
        return NGX_CONF_ERROR;
    }

    return conf;
}